prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = programa.cpp Pila.cpp Container.cpp
OBJ = programa.o Pila.o Container.o  
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

