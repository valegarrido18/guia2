>> PILAS(con arreglos)
	PUERTO SECO <<

EL programa se deberá ejecutar junto con dos parámetros los cuales indicaran la cantidad de pilas y el largo de la pila, respectivamente. Luego de ser ejecutado se mostrará un menu para ingresar datos a la pila, remover, ver la pila y salir. 


    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa agregando un parámetro que dará la dimensión del arreglo. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
