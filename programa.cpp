#include <iostream>
#include <stdlib.h>
#include "Pila.h"
#include "Container.h" 

using namespace std;


int main(int argc, char *argv[]){
	/* n es la cantidad de pilas y m es el largo de la pila */ 
	int n = stoi(argv[1]);
	int m = stoi(argv[2]);
	
	string opciones;

	Pila pila[m];
	/* validando arreglo */ 
	for (int i = 0; i < n; i++){
		pila[i].validar_vector(m);
	}	
	
	bool u = true;
	while (u){
		cout << "Agregar  [1]" << endl; 
		cout << "Remover  [2]" << endl;
		cout << "Ver pila [3]" << endl;
		cout << "Salir    [0]" << endl;
		cout << "----------------" << endl;
		cin >> opciones;
		if (opciones == "1" ){
			
			int x;
		
			cout << "Seleccione pila: " << endl; 
			cin >> x;
			
				
			pila[x-1].Push();
		}
		
		if (opciones == "2" ){
			
			int z;
			
			cout << "Eliminar pila: " << endl;
			cin >> z;
			
			pila[z-1].Pop();
		}
		
		if (opciones == "3" ){
			
			for (int i = n-1; i >= 0; i --){
				for (int z = 0; z < n; z++){
					pila[z].ver_datos(i);
					cout << "\t|";
				}
				cout << endl;
			}
			
		}
		
		if (opciones == "0" ){
			exit(0);
		
		}
	}
	return 0;
} 
